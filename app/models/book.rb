class Book < ActiveRecord::Base

  validates :title, presence: true
  validates :pages, numericality: { only_integer: true }
  has_attached_file :cover, :styles => { :medium => '400x400>', :thumb => '200x200>' },
                  :default_url => '/images/:style/missing.png'
  validates_attachment_content_type :cover, :content_type => /\Aimage\/.*\Z/
  validates_attachment_presence :cover

  belongs_to :user  
  has_many :reviews, dependent: :destroy
end
