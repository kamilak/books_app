class BooksControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  setup do
    @request.env["devise.mapping"] = Devise.mappings[:admin]
    sign_in FactoryGirl.create(:user)
    @book = create(:book)
  end

  test 'should get index' do
    get :index
    assert_response :success
    assert_select 'title', 'Books'
  end

  test 'should get show' do
    get :show, id: @book
    assert_response :success
    assert_select 'title', @book.title
  end

  test 'should get new' do
    get :new
    assert_response :success
    assert_select 'title', 'Add Book'
  end

  test 'should create book' do
    assert_difference('Book.count', 1) do
      post :create, book: attributes_for(:book)
    end
    assert_redirected_to book_path(assigns(:book))
    assert_equal 'Book added to your base!', flash[:notice]
  end

  test 'should get edit' do
    get :edit, id: @book
  end

  test 'should update book' do
    put :update, id: @book, book: attributes_for(:book)
    assert_redirected_to book_path(assigns(:book))
    assert_equal 'Book successfully updated.', flash[:notice]
  end

  test 'should destroy book' do
    assert_difference('Book.count', -1) do
      delete :destroy, id: @book
    end
    assert_redirected_to books_path
    assert_equal 'Book was successfully deleted.', flash[:notice]
  end
end
