class ReviewsControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  
  setup do
    @request.env["devise.mapping"] = Devise.mappings[:admin]
    sign_in FactoryGirl.create(:user)
    @book = create(:book)
    @review = create(:review)
  end

  test "should get new" do
    get :new, :book_id => @book.id 
    assert_response :success
  end
  
  test "should create review" do
    assert_difference('Review.count') do
      post :create, :book_id => @book.id, review: { comment: @review.comment, rating: @review.rating } 
    end

    assert_redirected_to book_path(assigns(:book))
  end
  
  test "should get edit" do
    get :edit, :book_id => @book.id, id: @review 
    assert_response :success
  end
  
  test "should update review" do
    patch :update, :book_id => @book.id, id: @review, review: { comment: @review.comment, rating: @review.rating }
    assert_redirected_to book_path(assigns(:book))
  end
  
    test "should destroy review" do
    assert_difference('Review.count', -1) do
      delete :destroy, :book_id => @book.id, id: @review
    end

    assert_redirected_to book_path(assigns(:book))
  end
end

