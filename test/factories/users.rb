FactoryGirl.define do

  factory :user do
    email "name.surname@example.com"
    password "password"
    password_confirmation { "password" }
  end
end
