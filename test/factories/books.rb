include ActionDispatch::TestProcess

FactoryGirl.define do

  factory :book do
    title   'Title'
    author  'Name Surname'
    pages   100
    cover   { fixture_file_upload('test/fixtures/paperclip/cover.jpg', 'image/jpeg') }
  end
end
