require 'test_helper'

class BookTest < ActiveSupport::TestCase

  setup do
    @book = create(:book)
  end
  
  test 'book should be valid' do
    assert @book.valid?
  end
  
  test 'title should be present' do
    @book.title = ''
    assert_not @book.valid?
  end

  test 'pages_no should be integer' do
    @book.pages = 'xyz'
    assert_not @book.valid?
  end
  
  test 'cover file name should be present' do
    @book.cover_file_name = ''
    assert_not @book.valid?
  end

  test 'cover type should be image' do
    @book.cover_content_type = 'xyz'
    assert_not @book.valid?
  end
end
